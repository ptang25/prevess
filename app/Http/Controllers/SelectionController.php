<?php

namespace App\Http\Controllers;

use App\Models\recipe_choices;
use App\Models\recipe_ingredients;
use App\Models\recipe_nutrients;
use App\Models\recipes;
use App\Models\user_daily_logs;
use App\Models\user_dish_logs;
use App\Models\user_fav_foods;
use App\Models\user_vitals;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class selectionController extends Controller
{
    /**
     * Save the Meal Choice selected by the user
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function saveMealChoice(Request $request)
    {
        $meal_date = $request->input('date');
        $meal_plan = $request->input('type');

        error_log($meal_date);
        error_log($meal_plan);

        //User
        $user = Auth::user();

        $new_meal_log = new user_daily_logs();
        $new_meal_log->userID = $user->id;
        $new_meal_log->active_date = date('Y-m-d', strtotime($meal_date));
        $new_meal_log->type = $meal_plan;
        $new_meal_log->save();
    }

    /**
     * Provides the User with Meal Options
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function foodOptions(Request $request)
    {
        $meal_date = $request->input('date');
        $meal_type = $request->input('type');

        //$meal_date = '2021-02-21';
        //$meal_type = "breakfast";

        //User
        $user = Auth::user();
        $date = date('Y-m-d', strtotime($meal_date));

        error_log($date);

        $user_meal_date_obj = user_daily_logs::where([['userID', '=', $user->id], ['active_date', '=', $date]])->first();

        $formula_consts = user_vitals::where([['userID', '=', $user->id]])->first();

        /*error_log("Meal Type: " . $meal_type);
        error_log("Metabo Rate: " . $formula_consts->metabo_rate);
        error_log("Carb: " . $formula_consts->diet->carbs);
        error_log("Sugar: " . $formula_consts->diet->sugar);
        error_log("Protein: " . $formula_consts->diet->protein);
        error_log("Fats: " . $formula_consts->diet->fats);
        error_log("Weight: " . $formula_consts->general->weight);*/
        
        $meal_weight = 1.0;

        if ($meal_type == "breakfast") {
            //Check what type of Meal was chosen
            if ($user_meal_date_obj->type == "3 Meals") {
                $meal_perc = 0.3;
            } 
            else if ($user_meal_date_obj->type == "3 Meals, 1 Snack") {
                $meal_perc = 0.25;
            }
            else {
                $meal_perc = 0.2;
            }      
        }
        else if ($meal_type == "lunch") {
            //Check what type of Meal was chosen
            if ($user_meal_date_obj->type == "3 Meals") {
                $meal_perc = 0.35;
            } 
            else if ($user_meal_date_obj->type == "3 Meals, 1 Snack") {
                $meal_perc = 0.3;
            }
            else {
                $meal_perc = 0.3;
            }      
        }
        else if ($meal_type == "dinner") {
            //Check what type of Meal was chosen
            if ($user_meal_date_obj->type == "3 Meals") {
                $meal_perc = 0.35;
            } 
            else if ($user_meal_date_obj->type == "3 Meals, 1 Snack") {
                $meal_perc = 0.3;
            }
            else {
                $meal_perc = 0.3;
            }      
        }
        else if ($meal_type == "snack1") {
            //Check what type of Meal was chosen
            if ($user_meal_date_obj->type == "3 Meals, 1 Snack") {
                $meal_perc = 0.15;
            }
            else {
                $meal_perc = 0.1;
            }
        }
        else if ($meal_type == "snack2") {
                //Check what type of Meal was chosen
                $meal_perc = 0.1;
        }
        
        $nutrients = array();

        $nutrients['carb'] = $meal_perc * ($formula_consts->metabo_rate * ($formula_consts->diet->carbs / 100) / 4) * $formula_consts->general->weight * $meal_weight; //4 is the KCal for Carbs
        $nutrients['sugar'] = $meal_perc *($formula_consts->metabo_rate * ($formula_consts->diet->carbs / 100) / 4) * ($formula_consts->diet->sugar / 100) * $formula_consts->general->weight * $meal_weight;
        $nutrients['protein'] = $meal_perc *($formula_consts->metabo_rate * ($formula_consts->diet->protein / 100) / 4) * $formula_consts->general->weight * $meal_weight; //4 is the Kcal for Protein
        $nutrients['fat'] = $meal_perc * ($formula_consts->metabo_rate * ($formula_consts->diet->fats / 100) / 9) * $formula_consts->general->weight * $meal_weight; //9 is the Kcal for fats
            
        $nutrients['sat'] =  $meal_perc * (0.31 * ($formula_consts->metabo_rate * ($formula_consts->diet->fats / 100) / 9)) * $formula_consts->general->weight * $meal_weight;
        $nutrients['mufa'] = $meal_perc * (0.433333333 * ($formula_consts->metabo_rate * ($formula_consts->diet->fats / 100) / 9)) * $formula_consts->general->weight * $meal_weight;
        $nutrients['pufa'] = $meal_perc *(0.233333333 * ($formula_consts->metabo_rate * ($formula_consts->diet->fats / 100) / 9)) * $formula_consts->general->weight * $meal_weight;
        $nutrients['trans'] =  $meal_perc * (0.233333333 * ($formula_consts->metabo_rate * ($formula_consts->diet->fats / 100) / 9)) * $formula_consts->general->weight * $meal_weight;

        $nutrients['salt'] = $meal_perc * 6 * $meal_weight;

        $nutrients['vita_min'] = $meal_perc * 0.5 * $meal_weight;
        $nutrients['vita_max'] = $meal_perc * 3 * $meal_weight;
        $nutrients['vitb1_min'] = $meal_perc * 0.75 * $meal_weight;
        $nutrients['vitb1_max'] = $meal_perc * 4 * $meal_weight;
        $nutrients['vitb2_min'] = $meal_perc * 1.3 * $meal_weight;
        $nutrients['vitb2_max'] = $meal_perc * 6 * $meal_weight;
        $nutrients['vitb3_min'] = $meal_perc * 14 * $meal_weight;
        $nutrients['vitb3_max'] = $meal_perc * 40 * $meal_weight;
        $nutrients['vitb5_min'] = $meal_perc * 3 * $meal_weight;
        $nutrients['vitb5_max'] = $meal_perc * 12.5 * $meal_weight;
        $nutrients['vitb6_min'] = $meal_perc * 1.3 * $meal_weight;
        $nutrients['vitb6_max'] = $meal_perc * 6 * $meal_weight;
        $nutrients['vitb7_min'] = $meal_perc * 18 * $meal_weight;   //ug
        $nutrients['vitb7_max'] = $meal_perc * 200 * $meal_weight;  //ug
        $nutrients['vitb9_min'] = $meal_perc * 140 * $meal_weight;  //ug
        $nutrients['vitb9_max'] = $meal_perc * 1000 * $meal_weight;  //ug
        $nutrients['vitb12_min'] = $meal_perc * 1 * $meal_weight; //ug
        $nutrients['vitb12_max'] = $meal_perc * 10 * $meal_weight;   //ug
        $nutrients['vitc_min'] = $meal_perc * 60 * $meal_weight; 
        $nutrients['vitc_max'] = $meal_perc * 300 * $meal_weight; 
        $nutrients['sodium_min'] = $meal_perc * 550 * $meal_weight; 
        $nutrients['sodium_max'] = $meal_perc * 3500 * $meal_weight; 
        $nutrients['potassium_min'] = $meal_perc * 2000 * $meal_weight; 
        $nutrients['potassium_max'] = $meal_perc * 6000 * $meal_weight; 
        $nutrients['calcium_min'] = $meal_perc * 580 * $meal_weight;
        $nutrients['calcium_max'] = $meal_perc * 2000 * $meal_weight;
        $nutrients['phosphorus_min'] = $meal_perc * 400 * $meal_weight;
        $nutrients['phosphorus_max'] = $meal_perc * 2500 * $meal_weight;
        $nutrients['magnesium_min'] = $meal_perc * 150 * $meal_weight;
        $nutrients['magnesium_max'] = $meal_perc * 1000 * $meal_weight;
        $nutrients['iron_min'] = $meal_perc * 7 * $meal_weight;
        $nutrients['iron_max'] = $meal_perc * 30 * $meal_weight;
        $nutrients['iodine_min'] = $meal_perc * 100 * $meal_weight; //ug
        $nutrients['iodine_max'] = $meal_perc * 500 * $meal_weight; //ug
        $nutrients['fluoride_min'] = $meal_perc * 0.6 * $meal_weight; 
        $nutrients['fluoride_max'] = $meal_perc * 4 * $meal_weight; 
        $nutrients['zinc_min'] = $meal_perc * 7 * $meal_weight; 
        $nutrients['zinc_max'] = $meal_perc * 25 * $meal_weight; 
        $nutrients['selenium_min'] = $meal_perc * 50 * $meal_weight; 
        $nutrients['selenium_max'] = $meal_perc * 400 * $meal_weight; 
        $nutrients['copper_min'] = $meal_perc * 1.8 * $meal_weight; 
        $nutrients['copper_max'] = $meal_perc * 6 * $meal_weight; 
        $nutrients['manganese_min'] = $meal_perc * 1 * $meal_weight; 
        $nutrients['manganese_max'] = $meal_perc * 10 * $meal_weight; 
        $nutrients['chrome_min'] = $meal_perc * 40 * $meal_weight;   //ug
        $nutrients['chrome_max'] = $meal_perc * 600 * $meal_weight;  //ug
        $nutrients['molybdenum_min'] = $meal_perc * 40 * $meal_weight;   //ug
        $nutrients['molybdenum_max'] = $meal_perc * 600 * $meal_weight;  //ug

    
        return $this->runEdamamAPI($nutrients, $user_meal_date_obj, $meal_type);

    }
    
    private function runEdamamAPI($nutrients, $user_meal_date_obj, $meal_type) {

        //The string values
        $food_type_const = "q=";
        $app_id = "app_id=". env('EDAMAM_ID');
        $app_key = "app_key=" . env('EDAMAM_KEY');
        $num_recipes = "from=0&to=9&";

        $fats = "nutrients%5BFAT%5D=";
        $proteins = "nutrients%5BPROCNT%5D=";
        $carbs = "nutrients%5BCHOCDF%5D=";
        $sugars = "nutrients%5BSUGAR%5D=";

        $sat = "nutrients%5FASAT%5D=";
        $mufa = "nutrients%5FAMS%5D=";
        $pufa = "nutrients%5FAPU%5D=";
        $trans = "nutrients%5FATRN%5D=";

        $vitamin_a = "nutrients%5VITA_RAE%5D=";
        $vitamin_b6 = "nutrients%5VITB6A%5D=";
        $vitamin_b12 = "nutrients%5VITB12%5D=";
        $vitamin_c = "nutrients%5VITC%5D=";
        $sodium = "nutrients%5NA%5D=";
        $potassium = "nutrients%5K%5D=";
        $magnesium = "nutrients%5MG%5D=";
        $potassium = "nutrients%5K%5D=";
        $calcium = "nutrients%5CA%5D=";
        $phosphorus = "nutrients%5P%5D=";
        $iron = "nutrients%5FE%5D=";

        //User
        $user = Auth::user();

        //Get Favorite foods
        $type_of_meal = $meal_type;
        
        if (($type_of_meal == "snack1") || ($type_of_meal == "snack2")) {
            $type_of_meal = "snack";
        }

        $fav_food = user_fav_foods::where([['userID', '=', $user->id], ['meal', '=', $type_of_meal]])->get();
        $fav_food = $fav_food->random(1);

        $link = "https://api.edamam.com/search?" . $food_type_const . $fav_food->food . "&" . $app_id . "&" . $app_key  . "&" .
                $num_recipes . $carbs . $nutrients['carb'] . "&" . $sugars . $nutrients['sugar'] . "&" . $proteins . $nutrients['protein'] . "&" . $fats . $nutrients['fat'] . "&" .
                $sat . $nutrients['sat'] . "&" . $mufa . $nutrients['mufa'] . "&" . $pufa . $nutrients['pufa'] . "&" . $trans . $nutrients['trans'] . "&" .
                $vitamin_a . $nutrients['vita_min'] . "-"  . $nutrients['vita_max'] . "&" .  $vitamin_b6 . $nutrients['vitb6_min'] . "-"  . $nutrients['vitb6_max'] . "&" . 
                $vitamin_b12 . $nutrients['vitb12_min'] . "-"  . $nutrients['vitb12_max'] . "&" . $vitamin_c . $nutrients['vitc_min'] . "-"  . $nutrients['vitc_max'] . "&" .
                $sodium . $nutrients['sodium_min'] . "-"  . $nutrients['sodium_max'] . "&" . $potassium . $nutrients['potassium_min'] . "-"  . $nutrients['potassium_max'] . "&" .
                $magnesium . $nutrients['magnesium_min'] . "-"  . $nutrients['magnesium_max'] . "&" . $calcium . $nutrients['calcium_min'] . "-"  . $nutrients['calcium_max'] . "&" .
                $phosphorus . $nutrients['phosphorus_min'] . "-"  . $nutrients['phosphorus_max'] . "&" . $iron . $nutrients['iron_min'] . "-"  . $nutrients['iron_max']; 

        error_log($link);        
        
        $response = Http::get($link);
        $edamam_obj = $response->json();

        $eda_recipes = $edamam_obj['hits'];

        foreach ($eda_recipes as $recipe) {
        
            //Get the url to determine duplicates
            $myrecipe = recipes::firstOrNew(
                ['title' =>  $recipe['recipe']['label']],
                ['image_url' => $recipe['recipe']['image']],
            );
            
            $myrecipe->url = $recipe['recipe']['url'];
            $myrecipe->source = $recipe['recipe']['source'];
            $myrecipe->yield =  $recipe['recipe']['yield'];
            $myrecipe->health = implode(",", $recipe['recipe']['healthLabels']);
            $myrecipe->diet =  implode(",", $recipe['recipe']['dietLabels']);
            $myrecipe->cautions = implode(",", $recipe['recipe']['cautions']);
            $myrecipe->calories =  $recipe['recipe']['calories'];
            $myrecipe->weight = $recipe['recipe']['totalWeight'];

            $myrecipe->save();

            //Save Individual Ingredients
            foreach ($recipe['recipe']['ingredients'] as $ingredients) {
                    
                $recipe_ingredient = recipe_ingredients::firstOrNew(
                    ['recipeID' => $myrecipe->id],
                    ['content' => $ingredients['text']]
                );
                    
                $recipe_ingredient->weight = $ingredients['weight'];
                $recipe_ingredient->save();
            }

            //Save Nutrient values
            foreach ($recipe['recipe']['digest'] as $nutrients) {

                    $tag = $nutrients['tag'];

                    $recipe_nutrient = recipe_nutrients::firstOrNew(
                        ['recipeID' => $myrecipe->id],
                        ['type' => $nutrients['label']],
                        
                    );

                    $recipe_nutrient->quantity = $nutrients['total'];
                    $recipe_nutrient->abbrev = $tag;
                    $recipe_nutrient->unit = $nutrients['unit'];
                    $recipe_nutrient->save();
            }
            

            //Save to Choices
            $user = Auth::user();

            $choices = new recipe_choices();
            $choices->userID = $user->id;
            $choices->recipeID = $myrecipe->id;
            $choices->dailyID = $user_meal_date_obj->id;
            $choices->type = $meal_type;
            $choices->save();
        }

        //Get the results of the Poll
        $recipe_choices = DB::table('recipe_choices')
                                ->join('recipes', 'recipes.id', '=', 'recipe_choices.recipeID')
                                ->select('recipes.id', 'recipes.title', 'recipes.image_url', 'recipes.calories', 'recipe_choices.dailyID')
                                ->where('recipe_choices.userID', $user->id)
                                ->where('type', $meal_type)
                                ->where('dailyID', $user_meal_date_obj->id)
                                ->get();

        $randomrecipes = $recipe_choices->random(3);

        return $randomrecipes;
    }

    /**
     * Save the Meal Choice selected by the user
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function selectMeal(Request $request)
    {

        $recipeID = $request->input('recipeID');
        $dailyID = $request->input('dailyID');
        $type = $request->input('type');

        $dish_log = new user_dish_logs();
        $dish_log->dailyID = $dailyID;
        $dish_log->recipeID = $recipeID;
        $dish_log->type = $type;
        $dish_log->save();

        //Remove all the other choices in DB
        $user = Auth::user();
        $deletedRows = recipe_choices::where('userID', $user->id)->delete();

        //Get the results of the Poll
        $dishes = DB::table('user_dish_logs')
                    ->join('recipes', 'recipes.id', '=', 'user_dish_logs.recipeID')
                    ->join('user_daily_logs', 'user_dish_logs.dailyID', '=', 'user_daily_logs.id')
                    ->select('recipes.title', 'recipes.image_url', 'recipes.url', 'user_dish_logs.type', 'user_daily_logs.type as meal_plan', 'user_daily_logs.active_date')
                    ->where('user_dish_logs.dailyID', $dailyID)
                    ->get();

        return $dishes;
    }

    /**
     * return Grocery List
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getList()
    {
        //User
        $user = Auth::user();

        $date = date('Y-m-d', strtotime("now"));

        //Get the results of the Poll
        $foods = DB::table('user_dish_logs')
                    ->join('recipe_ingredients', 'recipe_ingredients.recipeID', '=', 'user_dish_logs.recipeID')
                    ->join('user_daily_logs', 'user_dish_logs.dailyID', '=', 'user_daily_logs.id')
                    ->select('recipe_ingredients.content')
                    ->where('user_daily_logs.userID', $user->id)
                    ->where('user_daily_logs.active_date', $date)
                    ->get();

        return view('grocerylist', ['groceries' => $foods]);

    }
}
