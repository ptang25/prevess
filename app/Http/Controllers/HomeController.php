<?php

namespace App\Http\Controllers;

use App\Models\diet_goal;
use App\Models\general_goal;
use App\Models\nutritional_goal;
use App\Models\pal_multiplier;
use App\Models\physical_activity_level;
use App\Models\user_daily_logs;
use App\Models\user_dish_logs;
use App\Models\user_fav_foods;
use App\Models\user_vitals;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $user = Auth::user();

        $user_vitals = user_vitals::where([['userID', '=', $user->id]])->get();

        $pal = physical_activity_level::where([['userID', '=', $user->id]])->get();

        $fav = user_fav_foods::where([['userID', '=', $user->id]])->get();

        $toggle = "settings";

        if (isset($user_vitals) && (sizeof($user_vitals) > 0)) {

            $toggle = "pal";
        }
        if (isset($pal) && (sizeof($pal) > 0)) {

            $toggle = "fav";
        }
        if (isset($fav) && (sizeof($fav) > 0)) {

            $toggle = "calendar";
        }

        //Get type of General Goals
        $general_goals = general_goal::all();

        //Get Diet Goals
        $diet_goals = diet_goal::all();

        //Get Nutritional Goals
        $nutritional_goals = nutritional_goal::all();
                
        //Get user current general settings
        if ($toggle == "settings") {
            return view('home', ['general' => $general_goals, 'diet' => $diet_goals , 'nutrition' => $nutritional_goals,
                                'meals' => '', 'breakfast' => '', 'lunch' => '', 'dinner' => '',
                                'snack1' => '', 'snack2' => '', 'toggle' => "settings"]);
        }
        else if ($toggle == "pal") {
            return view('home', ['general' => $general_goals, 'diet' => $diet_goals , 'nutrition' => $nutritional_goals,   
                                'meals' => '', 'breakfast' => '', 'lunch' => '', 'dinner' => '',
                                'snack1' => '', 'snack2' => '', 'toggle' => "pal"]);
        }
        else if ($toggle == "fav") {
            return view('home', ['general' => $general_goals, 'diet' => $diet_goals , 'nutrition' => $nutritional_goals, 
                                'meals' => '', 'breakfast' => '', 'lunch' => '', 'dinner' => '',
                                'snack1' => '', 'snack2' => '', 'toggle' => "fav"]);
        }
        else {
            return $this->showCal();
        }
    }

    /**
     * Load the Settings
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function loadSettings()
    {

        $user = Auth::user();

        $user_vitals = user_vitals::where([['userID', '=', $user->id]])->first();

        $toggle = "settings";

        //Get type of General Goals
        $general_goals = general_goal::all();

        //Get Diet Goals
        $diet_goals = diet_goal::all();

        //Get Nutritional Goals
        $nutritional_goals = nutritional_goal::all();

        //Get user current general settings
        return view('home', ['general' => $general_goals, 'diet' => $diet_goals , 'nutrition' => $nutritional_goals, 
                            'settings' => $user_vitals,
                            'meals' => '', 'breakfast' => '', 'lunch' => '', 'dinner' => '',
                            'snack1' => '', 'snack2' => '', 'toggle' => $toggle]);
    }


    /**
     * Save the Settings for the User
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function saveSettings(Request $request)
    {

        $userID = $request->input('userID');
        $birthdate = $request->input('birthdate');
        $weight = $request->input('weight');
        $height = $request->input('height');
        $gender = $request->input('gender');
        $general = $request->input('gn');
        $nutritional = $request->input('ng');
        $diet = $request->input('dg');

        $user = Auth::user();

        $newdate = date('Y-m-d', strtotime($birthdate));

        //Get General Goal ID
        $gid = general_goal::where([['type', '=', $general]])->first();

        //Get Nutritional Goal ID
        $did = diet_goal::where([['type', '=', $diet]])->first();

        //Get Diet Goal ID
        $nid = nutritional_goal::where([['type', '=', $nutritional]])->first();

        if (isset($userID)) {
            $user_vitals = user_vitals::where([['userID', '=', $userID]])->first();
        }
        else {
            $user_vitals = new user_vitals();
            $user_vitals->userID = $user->id;
        }
        
        $user_vitals->birthdate = $newdate;
        $user_vitals->weight = $weight;
        $user_vitals->height = $height;
        $user_vitals->gender = $gender;
        $user_vitals->general_goal = $gid->id;
        $user_vitals->nutritional_goal = $nid->id;
        $user_vitals->diet_goal = $did->id;
        $user_vitals->save();

        return $this->index();
    }

     /**
     * Load the PAL for the User
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function loadPal()
    {

        $user = Auth::user();

        $pal = physical_activity_level::where([['userID', '=', $user->id]])->first();
        
        $toggle = "pal";

        //Get user current general settings
        return view('home', ['pal' => $pal,
                            'meals' => '', 'breakfast' => '', 'lunch' => '', 'dinner' => '',
                            'snack1' => '', 'snack2' => '', 'toggle' => $toggle]);
    }


    /**
     * Save the Settings for the User
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function savePal(Request $request)
    {

        $sleep = $request->input('sleep');
        $lying = $request->input('lying');
        $work = $request->input('work');
        $work_walk = $request->input('work_walk');
        $walk = $request->input('walk');
        $active = $request->input('active');

        $user = Auth::user();

        $pal = physical_activity_level::where([['userID', '=', $user->id]])->get();

        if (isset($pal) && sizeof($pal) > 0) {
            //first element
            $pal = $pal[0];
        }
        else {
            $pal = new physical_activity_level();
        }

        //Save to Database
        $pal->userID = $user->id;
        $pal->sleep = $sleep;
        $pal->lying = $lying;
        $pal->sedentary_work = $work;    
        $pal->sedentary_work_walk = $work_walk;        
        $pal->walk_stand = $walk;
        $pal->active = $active;
        $pal->save();

        //Get multipliers
        $pal_multiplier = pal_multiplier::limit(1)->first();

        //Calculate PAL multiplier
        $daily_pal = ($pal_multiplier->sleep * $pal->sleep + $pal_multiplier->lying * $pal->lying +
                               $pal_multiplier->sedentary_work * $pal->sedentary_work + $pal_multiplier->sedentary_work_walk * $pal->sedentary_work_walk +
                               $pal_multiplier->walk_stand * $pal->walk_stand + $pal_multiplier->active * $pal->active) / 24;
        
        //Get gender
        $user_vitals = user_vitals::where([['userID', '=', $user->id]])->first();

        //Get Age
        $age = date_diff(date_create($user_vitals->birthdate), date_create('today'))->y;

        //Basal Metabolism Rate
        $basal_metabo_rate = 0.0;

        if ($user_vitals->gender == "female") {
            $basal_metabo_rate = 655 + (9.563 * $user_vitals->weight) + (1.850 * $user_vitals->height) - (4.676 * $age);
        } 
        else if ($user_vitals->gender == "male") {
            $basal_metabo_rate = 66.5 + (13.75 * $user_vitals->weight) + (5.003 * $user_vitals->height) - (6.755 * $age);
        }         

        //Daily Metabolism RAte
        $metabo_rate = $daily_pal * $basal_metabo_rate;
        
        $user_vitals->basal_metabo_rate = $basal_metabo_rate;
        $user_vitals->metabo_rate = $metabo_rate;

        $user_vitals->save();

        return $this->index();
    }

    /**
     * Save the Settings for the User
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showCal()
    {
        $user = Auth::user();

        $dayofweek = date('w', strtotime("now"));
        $day = date('d', strtotime("now"));
        $month = date('F', strtotime("now"));
        $year = date('y', strtotime("now"));

        //Is it Sunday?
        if ($dayofweek == 0) {
            $last_sunday = date('Y-m-d', strtotime('now'));
            $next_sunday = date('Y-m-d', strtotime('next saturday + 1 week', strtotime('now')));
        }
        else {
            $last_sunday = date('Y-m-d', strtotime('last Sunday', strtotime('now')));
            $next_sunday = date('Y-m-d', strtotime('next saturday + 1 week', strtotime('now')));
        }

        //Get any User defined Meal Plans
        $meal_plans = user_daily_logs::where([['userID', '=', $user->id]])
                                     ->whereBetween('active_date', [$last_sunday, $next_sunday])
                                     ->get();
        
        $meal_array = [];

        foreach ($meal_plans as $meal_plan) {

            $meal_array[$meal_plan->active_date] = $meal_plan->type;
            $meal_array_id[$meal_plan->id] = $meal_plan->active_date;  //For use with Meal logs
        }

        //Get just the Meal IDs
        $mealIDs = $meal_plans->pluck(["id"])->all();

        $meal_logs = DB::table('user_dish_logs')
                    ->join('recipes', 'recipes.id', '=', 'user_dish_logs.recipeID')
                    ->join('user_daily_logs', 'user_dish_logs.dailyID', '=', 'user_daily_logs.id')
                    ->select('recipes.title', 'recipes.image_url', 'recipes.url', 'user_dish_logs.type', 'user_daily_logs.type as meal_plan', 'user_daily_logs.active_date', 'user_dish_logs.dailyID')
                    ->whereIn('user_dish_logs.dailyID', $mealIDs)
                    ->get();

        /*$meal_logs = user_dish_logs::whereIn('dailyID', $mealIDs)
                                    ->get();*/

        //Breakfast Array
        $temp_array = $meal_logs->filter(function ($item) {
                        return $item->type == "breakfast";
                    })->values();

        $breakfast_array = [];

        foreach ($temp_array as $breakfast) {
            $breakfast_array[$meal_array_id[$breakfast->dailyID]] = $breakfast;
        }
        
        //Lunch Array
        $temp_array = $meal_logs->filter(function ($item) {
                        return $item->type == "lunch";
                    })->values();

        $lunch_array = [];

        foreach ($temp_array as $lunch) {
            $lunch_array[$meal_array_id[$lunch->dailyID]] = $lunch;
        }

        //Dinner Array
        $temp_array = $meal_logs->filter(function ($item) {
                        return $item->type == "dinner";
                    })->values();
        $dinner_array = [];

        foreach ($temp_array as $dinner) {
            $dinner_array[$meal_array_id[$dinner->dailyID]] = $dinner;
        }

        //Snack 1 Array
        $temp_array = $meal_logs->filter(function ($item) {
                        return $item->type == "snack1";
                    })->values();
        $snack1_array = [];

        foreach ($temp_array as $snack1) {
            $snack1_array[$meal_array_id[$snack1->dailyID]] = $snack1;
        }

        //Snack 2 Array
        $temp_array = $meal_logs->filter(function ($item) {
                        return $item->type == "snack2";
                    })->values();
        $snack2_array = [];

        foreach ($temp_array as $snack2) {
            $snack2_array[$meal_array_id[$snack2->dailyID]] = $snack2;
        }
        
        return view('home', ['dayofweek' => $dayofweek, 'day' => $day, 'month' => $month, 'year' => $year, 
                            'meals' => $meal_array, 'breakfast' => $breakfast_array, 'lunch' => $lunch_array, 'dinner' => $dinner_array,
                            'snack1' => $snack1_array, 'snack2' => $snack2_array, 'toggle' => "calendar"]);

    }

    /**
     * Load the Settings
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function loadFoods()
    {
        $user = Auth::user();

        $fav_foods = user_fav_foods::where([['userID', '=', $user->id]])->get();
        
        $toggle = "fav";

        //Breakfast
        $filtered_breakfast = $fav_foods->filter(function ($item) {
            return $item->meal == "breakfast";
        })->values();

        $breakfast = $filtered_breakfast->pluck(["food"])->all();
        $breakfast = implode(",", $breakfast);

        //Lunch
        $filtered_lunch = $fav_foods->filter(function ($item) {
            return $item->meal == "lunch";
        })->values();

        $lunch = $filtered_lunch->pluck(["food"])->all();
        $lunch = implode(",", $lunch);

        //Dinner
        $filtered_dinner = $fav_foods->filter(function ($item) {
            return $item->meal == "dinner";
        })->values();

        $dinner = $filtered_dinner->pluck(["food"])->all();
        $dinner = implode(",", $dinner);

        //Snack
        $filtered_snack = $fav_foods->filter(function ($item) {
            return $item->meal == "snack";
        })->values();

        $snack = $filtered_snack->pluck(["food"])->all();
        $snack = implode(",", $snack);

        //Get user current general settings
        return view('home', ['breakfast_fav' => $breakfast, 'lunch_fav' => $lunch, 'dinner_fav' => $dinner, 'snack_fav' => $snack, 
                            'meals' => '', 'breakfast' => '', 'lunch' => '', 'dinner' => '',
                            'snack1' => '', 'snack2' => '', 'toggle' => $toggle]);
    }

     /**
     * Save the Settings for the User
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function saveFoods(Request $request)
    {

        $breakfast = $request->input('breakfast');
        $lunch = $request->input('lunch');
        $dinner = $request->input('dinner');
        $snack = $request->input('snack');

        $user = Auth::user();

        $myBreakfastArray = explode(',', $breakfast);
        $myLunchArray = explode(',', $lunch);
        $myDinnerArray = explode(',', $dinner);
        $mySnackArray = explode(',', $snack);

        //Save all Breakfast favs
        foreach ($myBreakfastArray as $breakfast_favs) {

            $breakfast_favs = trim($breakfast_favs);

            //Get the url to determine duplicates
            $breakfast = user_fav_foods::firstOrNew(
                ['userID' =>  $user->id,
                'food' => $breakfast_favs]
            );

            $breakfast->meal = "breakfast";
            $breakfast->save();
        }

        //Save all Lunch favs
        foreach ($myLunchArray as $lunch_favs) {

            $lunch_favs = trim($lunch_favs);

            //Get the url to determine duplicates
            $lunch = user_fav_foods::firstOrNew(
                ['userID' =>  $user->id,
                'food' => $lunch_favs],
            );

            $lunch->meal = "lunch";
            $lunch->save();    
        }

        //Save all dinner ingredients
        foreach ($myDinnerArray as $dinner_favs) {

            $dinner_favs = trim($dinner_favs);

            //Get the url to determine duplicates
            $dinner = user_fav_foods::firstOrNew(
                ['userID' =>  $user->id,
                'food' => $dinner_favs],
            );

            $dinner->meal = "dinner";
            $dinner->save();    
        }

        //Save all snack ingredients
        foreach ($mySnackArray as $snack_favs) {

            $snack_favs = trim($snack_favs);

            //Get the url to determine duplicates
            $snack = user_fav_foods::firstOrNew(
                ['userID' =>  $user->id,
                'food' => $snack_favs],
            );

            $snack->meal = "snack";
            $snack->save();    
        }

        return $this->index();
    }

    /**
     * Save the Settings for the User
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showCaleDate($date)
    {
        $user = Auth::user();

        $dayofweek = date('w', strtotime("now"));
        $day = date('d', strtotime("now"));
        $month = date('F', strtotime("now"));
        $year = date('y', strtotime("now"));

        //Is it Sunday?
        if ($dayofweek == 0) {
            $last_sunday = date('Y-m-d', strtotime('now'));
            $next_sunday = date('Y-m-d', strtotime('next saturday + 1 week', strtotime('now')));
        }
        else {
            $last_sunday = date('Y-m-d', strtotime('last Sunday', strtotime('now')));
            $next_sunday = date('Y-m-d', strtotime('next saturday + 1 week', strtotime('now')));
        }

        //Get any User defined Meal Plans
        $meal_plans = user_daily_logs::where([['userID', '=', $user->id]])
                                     ->whereBetween('active_date', [$last_sunday, $next_sunday])
                                     ->get();

        $meal_array = [];

        foreach ($meal_plans as $meal_plan) {

            $meal_array[$meal_plan->active_date] = $meal_plan->type;
            $meal_array_id[$meal_plan->id] = $meal_plan->active_date;  //For use with Meal logs
        }

        //Get any User selected recipes

        //Get just the Meal IDs
        $mealIDs = $meal_plans->pluck(["id"])->all();

        $meal_logs = user_dish_logs::whereIn('dailyID', $mealIDs)
                                    ->get();

        //Breakfast Array
        $temp_array = $meal_logs->filter(function ($item) {
                        return $item->type == "breakfast";
                    })->values();

        $breakfast_array = [];

        foreach ($temp_array as $breakfast) {
            $breakfast_array[$meal_array_id[$breakfast->dailyID]] = $breakfast->recipeID;
        }
        
        //Lunch Array
        $temp_array = $meal_logs->filter(function ($item) {
                        return $item->type == "lunch";
                    })->values();

        $lunch_array = [];

        foreach ($temp_array as $lunch) {
            $lunch_array[$meal_array_id[$lunch->dailyID]] = $lunch->recipeID;
        }

        //Dinner Array
        $temp_array = $meal_logs->filter(function ($item) {
                        return $item->type == "dinner";
                    })->values();
        $dinner_array = [];

        foreach ($temp_array as $dinner) {
            $dinner_array[$meal_array_id[$dinner->dailyID]] = $dinner->recipeID;
        }

        //Snack 1 Array
        $temp_array = $meal_logs->filter(function ($item) {
                        return $item->type == "snack1";
                    })->values();
        $snack1_array = [];

        foreach ($temp_array as $snack1) {
            $snack1_array[$meal_array_id[$snack1->dailyID]] = $snack1->recipeID;
        }

        //Snack 2 Array
        $temp_array = $meal_logs->filter(function ($item) {
                        return $item->type == "snack2";
                    })->values();
        $snack2_array = [];

        foreach ($temp_array as $snack2) {
            $snack2_array[$meal_array_id[$snack2->dailyID]] = $snack2->recipeID;
        }
        
        return view('home', ['dayofweek' => $dayofweek, 'day' => $day, 'month' => $month, 'year' => $year, 
                            'meals' => $meal_array, 'breakfast' => $breakfast_array, 'lunch' => $lunch_array, 'dinner' => $dinner_array,
                            'snack1' => $snack1_array, 'snack2' => $snack2_array, 'toggle' => "calendar", 'date' => $date]);

    }

}
