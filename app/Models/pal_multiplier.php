<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pal_multiplier extends Model
{
    use HasFactory;

    protected $table = 'pal_multipliers';

    protected $fillable = ['sleep', 'lying', 'seditary_work', 'walk_stand', 'active'];
}
