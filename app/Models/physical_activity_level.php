<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class physical_activity_level extends Model
{
    use HasFactory;

    protected $table = 'physical_activity_levels';

    protected $fillable = ['sleep', 'lying', 'seditary_work', 'walk_stand', 'active'];
}
