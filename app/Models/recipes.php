<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class recipes extends Model
{
    use HasFactory;

    protected $table = 'recipes';

    protected $fillable = ['title', 'image_url', 'url', 'source', 'yield', 'diet', 'health', 'cautions', 'calories', 'weight'];
}
