<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class user_fav_foods extends Model
{
    use HasFactory;

    protected $table = 'user_fav_foods';

    protected $fillable = ['userID', 'food'];
}
