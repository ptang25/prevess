<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class diet_goal extends Model
{
    use HasFactory;

    protected $table = 'diet_goals';

    protected $fillable = ['type', 'carbs', 'sugar', 'protein', 'fats'];
}
