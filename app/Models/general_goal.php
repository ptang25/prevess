<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class general_goal extends Model
{
    use HasFactory;

    protected $table = 'general_goals';

    protected $fillable = ['type', 'weight'];
}
