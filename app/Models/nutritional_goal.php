<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class nutritional_goal extends Model
{
    use HasFactory;

    protected $table = 'nutritional_goals';

    protected $fillable = ['type', 'weight'];
}
