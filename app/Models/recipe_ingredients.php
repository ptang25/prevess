<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class recipe_ingredients extends Model
{
    use HasFactory;

    protected $table = 'recipe_ingredients';

    protected $fillable = ['recipeID', 'content', 'weight'];
}
