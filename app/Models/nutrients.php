<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class nutrients extends Model
{
    use HasFactory;

    protected $table = 'nutrients';

    protected $fillable = ['type', 'low', 'optimal', 'high'];
}
