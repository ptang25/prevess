<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class recipe_choices extends Model
{
    use HasFactory;

    protected $table = 'recipe_choices';

    protected $fillable = ['userID', 'dailyID', 'type', 'recipeID'];

     /**
     * Get Nutritional Goal
     */
    public function recipes()
    {
        return $this->hasOne(recipes::class, 'id', 'recipeID');
    }

}
