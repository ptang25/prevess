<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class user_dish_logs extends Model
{
    use HasFactory;

    protected $table = 'user_dish_logs';

    protected $fillable = ['dailyID', 'recipeID', 'type'];
}
