<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class recipe_dailyrec_nutrients extends Model
{
    use HasFactory;

    protected $table = 'recipe_dailyrec_nutrients';

    protected $fillable = ['recipeID', 'type', 'abbrev', 'quantity', 'percentage'];
}
