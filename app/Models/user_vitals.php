<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class user_vitals extends Model
{
    use HasFactory;

    protected $table = 'user_vitals';

    protected $fillable = ['birthdate', 'gender', 'weight', 'height', 'general_goal', 'nutritional_goal', 'diet_goal', 'basal_metabo_rate', 'metabo_rate'];

    /**
     * Get Diet Goal
     */
    public function diet()
    {
        return $this->hasOne(diet_goal::class, 'id', 'diet_goal');
    }

    /**
     * Get Nutritional Goal
     */
    public function nutrition()
    {
        return $this->hasOne(nutritional_goal::class, 'id', 'nutritional_goal');
    }

    /**
     * Get Diet Goal
     */
    public function general()
    {
        return $this->hasOne(general_goal::class, 'id', 'general_goal');
    }
}
