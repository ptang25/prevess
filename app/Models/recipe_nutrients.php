<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class recipe_nutrients extends Model
{
    use HasFactory;

    protected $table = 'recipe_nutrients';

    protected $fillable = ['recipeID', 'type', 'abbrev', 'quantity', 'unit'];
}
