<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/loadSettings', [App\Http\Controllers\HomeController::class, 'loadSettings'])->middleware('auth');
Route::post('/saveSettings', [App\Http\Controllers\HomeController::class, 'saveSettings'])->middleware('auth');

Route::get('/loadPAL', [App\Http\Controllers\HomeController::class, 'loadPal'])->middleware('auth');
Route::post('/savePAL', [App\Http\Controllers\HomeController::class, 'savePal'])->middleware('auth');

Route::get('/loadFoods', [App\Http\Controllers\HomeController::class, 'loadFoods'])->middleware('auth');
Route::post('/saveFoods', [App\Http\Controllers\HomeController::class, 'saveFoods'])->middleware('auth');

Route::get('/showCal', [App\Http\Controllers\HomeController::class, 'showCal'])->middleware('auth');

Route::post('/selectMealPlan', [App\Http\Controllers\SelectionController::class, 'saveMealChoice'])->middleware('auth');
Route::post('/r_breakfast', [App\Http\Controllers\SelectionController::class, 'foodOptions'])->middleware('auth');

Route::post('/select_meal', [App\Http\Controllers\SelectionController::class, 'selectMeal'])->middleware('auth');

Route::get('/groceryList', [App\Http\Controllers\SelectionController::class, 'getList'])->middleware('auth');





