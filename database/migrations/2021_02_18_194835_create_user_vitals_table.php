<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserVitalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_vitals', function (Blueprint $table) {
            $table->id();
            $table->integer("userID");
            $table->date("birthdate");
            $table->string("gender");
            $table->integer("weight");
            $table->integer("height");
            $table->integer("general_goal");
            $table->integer("nutritional_goal");
            $table->integer("diet_goal");
            $table->double("basal_metabo_rate");
            $table->double("metabo_rate");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_vitals');
    }
}
