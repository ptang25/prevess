<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecipeDailyrecNutrientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipe_dailyrec_nutrients', function (Blueprint $table) {
            $table->id();
            $table->integer('recipeID');
            $table->string('type');
            $table->string('abbrev');
            $table->integer('quantity');
            $table->double('percentage');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipe_dailyrec_nutrients');
    }
}
