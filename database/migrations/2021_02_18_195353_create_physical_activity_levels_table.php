<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePhysicalActivityLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('physical_activity_levels', function (Blueprint $table) {
            $table->id();
            $table->integer('userID');
            $table->double('sleep');
            $table->double('lying');
            $table->double('sedentary_work');
            $table->double('seditary_work_walk');
            $table->double('walk_stand');
            $table->double('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('physical_activity_levels');
    }
}
