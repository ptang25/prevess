<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePalMultipliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pal_multipliers', function (Blueprint $table) {
            $table->id();
            $table->double('sleep');
            $table->double('lying');
            $table->double('seditary_work');
            $table->double('seditary_work_walk');
            $table->double('walk_stand');
            $table->double('active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pal_multipliers');
    }
}
