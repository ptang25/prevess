@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-md-8">
                            Welcome {{ Auth::user()->name }}
                        </div>
                        <div class="col-md-1">
                            <a href="/loadSettings"><i class="fa fa-cogs fa-2x"></i></a>
                        </div>
                        <div class="col-md-1">
                            <a href="/loadPAL"><i class="fa fa-walking fa-2x"></i></a>
                        </div>
                        <div class="col-md-1">
                            <a href="/loadFoods"><i class="fa fa-utensils fa-2x"></i></a>
                        </div>
                        <div class="col-md-1">
                            <a href="/groceryList"><i class="fa fa-list fa-2x"></i></a>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    @if ($toggle == "settings") 
        <div class="row justify-content-center">
            <div class="col-md-12">
                <form method="POST" enctype="multipart/form-data" id="upload_resume" action="/saveSettings">
                    @csrf        
                    @isset($settings)
                        <input type="hidden" id="userID" name="userID" value="{{ $settings->userID }}">
                    @endisset   
                    <div class="upload centercomponent mt-3">
                        <div class="upload-files">
                            <header>
                                <p>
                                    <i class="fa fa-cogs" aria-hidden="true"></i>
                                    <span class="up">Set</span>
                                    <span class="load">tings</span>
                                </p>
                            </header>
                    
                            <!-- Weight -->
                            <div class="row">
                                <input type="text" name="birthdate" class="question mt-2" id="birthdate" required autocomplete="off"
                                @isset($settings)
                                    value= {{ $settings->birthdate }}
                                @endisset
                                /> 
                                <label for="birthdate">
                                    <span>Your Birthdate (yyyy-mm-dd)</span>
                                </label>
                            </div>

                            <!-- Weight -->
                            <div class="row">
                                <input type="text" name="weight" class="question mt-2" id="weight" required autocomplete="off"
                                @isset($settings)
                                    value= {{ $settings->weight }}
                                @endisset
                                /> 
                                <label for="weight">
                                    <span>Your Weight (Kgs)</span>
                                </label>
                            </div>

                            <!-- Height -->
                            <div class="row">
                                <input type="text" name="height" class="question mt-2" id="height" required autocomplete="off"
                                @isset($settings)
                                    value= {{ $settings->height }}
                                @endisset
                                /> 
                                <label for="height">
                                    <span>Your Height (cm)</span>
                                </label>
                            </div>

                            <div class="row centercomponent mt-2">
                                    <label for="gender" class="profession_title">Birth Gender:</label>
                            
                                    <select id="gender" name="gender" class="selectComponentReg">
                                            <option value='male'
                                            @isset($settings)
                                                @if ($settings->gender == "male")
                                                    selected
                                                @endif
                                            @endisset
                                            >Male</option>
                                            <option value='female'
                                            @isset($settings)
                                                @if ($settings->gender == "female")
                                                    selected
                                                @endif
                                            @endisset
                                            >Female</option>
                                    </select>
                            </div>

                            <div class="row centercomponent">
                                <div class="col-md-5">
                                    <label for="gn" class="gn">General Goal:</label>
                                </div>
                                <div class="col-md-7">
                                    <select id="gn" name="gn" class="selectComponentReg">
                                            @foreach ($general as $type)
                                                <option value='{{ $type->type }}'
                                                @isset($settings)
                                                    @if ($settings->general_goal == $type->id)
                                                        selected
                                                    @endif
                                                @endisset
                                                >{{ $type->type }}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row centercomponent">
                                <div class="col-md-5">
                                    <label for="ng" class="profession_title">Nutrition Goal:</label>
                                </div>
                                <div class="col-md-7">
                                    <select id="ng" name="ng" class="selectComponentReg">
                                            @foreach ($nutrition as $type)
                                                <option value='{{ $type->type }}'
                                                @isset($settings)
                                                    @if ($settings->nutritional_goal == $type->id)
                                                        selected
                                                    @endif
                                                @endisset
                                                >{{ $type->type }}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row centercomponent">
                                <div class="col-md-5">
                                    <label for="dg" class="profession_title">Diet Goal:</label>
                                </div>
                                <div class="col-md-7">
                                    <select id="dg" name="dg" class="selectComponentReg">
                                            @foreach ($diet as $type)
                                                <option value='{{ $type->type }}'
                                                @isset($settings)
                                                    @if ($settings->diet_goal == $type->id)
                                                        selected
                                                    @endif
                                                @endisset  
                                                >{{ $type->type }}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row justify-content-center">
                                <input type="submit" value="Save Settings" onclick="check_errors(event)">
                            </div>

                            <div class="row justify-content-center">
                                <input type="button" value="Cancel" onclick="goBack()">
                            </div>

                        </div>
                    </div>
                </form> 
            </div>
        </div>  
        @elseif ($toggle == "pal") 
        <div class="row justify-content-center">
            <div class="col-md-12">
                <form method="POST" enctype="multipart/form-data" id="upload_resume" action="/savePAL">
                    @csrf           
                    @isset($pal)
                        <input type="hidden" id="userID" name="userID" value="{{ $pal->userID }}">
                    @endisset   
                    <div class="upload centercomponent mt-3">
                        <div class="upload-files">
                            <header>
                                <p>
                                    <i class="fa fa-walking" aria-hidden="true"></i>
                                    <span class="up">Physical</span>
                                    <span class="load">Activity Level</span>
                                </p>
                            </header>
                    
                            <!-- Sleep -->
                            <div class="row">
                                <input type="text" name="sleep" class="question mt-2" id="sleep" required autocomplete="off"
                                @isset($pal)
                                    value = {{ $pal->sleep }}
                                @endisset
                                /> 
                                <label for="sleep">
                                    <span>Sleep in a day (hr)</span>
                                </label>
                            </div>

                            <!-- Lying Around -->
                            <div class="row">
                                <input type="text" name="lying" class="question mt-2" id="lying" required autocomplete="off"
                                @isset($pal)
                                    value = {{ $pal->lying }}
                                @endisset
                                /> 
                                <label for="lying">
                                    <span>Lying around in a day (hr)</span>
                                </label>
                            </div>

                            <!-- Work -->
                            <div class="row">
                                <input type="text" name="work" class="question mt-2" id="work" required autocomplete="off"
                                @isset($pal)
                                    value = {{ $pal->sedentary_work }}
                                @endisset
                                /> 
                                <label for="work">
                                    <span>Sedentary work, no activity (hr)</span>
                                </label>
                            </div>

                            <!-- Work Walk -->
                             <div class="row">
                                <input type="text" name="work_walk" class="question mt-2" id="work_walk" required autocomplete="off"
                                @isset($pal)
                                    value = {{ $pal->sedentary_work_walk }}
                                @endisset
                                /> 
                                <label for="work_walk">
                                    <span>Sedentary work, limited walking (hr)</span>
                                </label>
                            </div>

                            <!-- Walking -->
                            <div class="row">
                                <input type="text" name="walk" class="question mt-2" id="walk" required autocomplete="off"
                                @isset($pal)
                                    value = {{ $pal->walk_stand }}
                                @endisset
                                /> 
                                <label for="walk">
                                    <span>Walking or standing a day (hr)</span>
                                </label>
                            </div>

                            <!-- Active -->
                            <div class="row">
                                <input type="text" name="active" class="question mt-2" id="active" required autocomplete="off"
                                @isset($pal)
                                    value = {{ $pal->active }}
                                @endisset
                                /> 
                                <label for="active">
                                    <span>Active leisure a day (hr)</span>
                                </label>
                            </div>

                            <div class="row justify-content-center">
                                <input type="submit" value="Save PAL settings" onclick="check_errors(event)">
                            </div>

                            <div class="row justify-content-center">
                                <input type="button" value="Cancel" onclick="goBack()">
                            </div>

                        </div>
                    </div>
                </form> 
            </div>
        </div>
        <!-- The User has gone through onboarding !-->
        @elseif ($toggle == "calendar")
            <div class="row justify-content-center">
                <div class="col-md-12 mt-3">
                    <div class="card">
                        <div class="card-header">{{ __('Weekly Meals') }}</div>
                        <div class="card-body">
                                @for($week = 0; $week < 2; $week++)
                                    <div class="row calendar_container">
                                        @for($days = 0; $days < 7; $days++)
                                            <div class="col" onclick="showmeals('{{ date('l', mktime(0, 0, 0, date('m'), $day - $dayofweek + $days, $year)) }}', '{{ $month }}', '{{ $day - $dayofweek + $days + ($week * 7)}}', '{{ cal_days_in_month(CAL_GREGORIAN, date('m'), $year) }}')">
                                                <div class="my-super-cool-btn">
                                                    <div class="dayofweek">
                                                        @if ( cal_days_in_month(CAL_GREGORIAN, date("m"), $year) >= ($day - $dayofweek + $days + ($week * 7)))
                                                            <p>{{ substr(date("l", mktime(0, 0, 0, date("m"), $day - $dayofweek + $days + ($week * 7), $year)), 0, 3) }} </p>
                                                        @else
                                                            <p>{{ substr(date("l", mktime(0, 0, 0, date("m"), ($day - $dayofweek + $days + ($week * 7)) - cal_days_in_month(CAL_GREGORIAN, date("m"), $year), $year)), 0, 3) }} </p>
                                                        @endif
                                                    </div>
                                                    <div class="day">
                                                        @if ( cal_days_in_month(CAL_GREGORIAN, date("m"), $year) >= ($day - $dayofweek + $days + ($week * 7)))
                                                            @if (($day - $dayofweek + $days + ($week * 7)) <= 0)
                                                                <p>{{ cal_days_in_month(CAL_GREGORIAN, date('m', strtotime('-1 month')), $year) + $day - $dayofweek + $days + ($week * 7) }}</p>
                                                            @else
                                                                <p>{{ $day - $dayofweek + $days + ($week * 7) }}</p>
                                                            @endif
                                                        @else
                                                            <p>{{ ($day - $dayofweek + $days + ($week * 7)) - cal_days_in_month(CAL_GREGORIAN, date("m"), $year) }}</p>
                                                        @endif    
                                                    </div>
                                                </div>
                                            </div> 
                                        @endfor
                                    </div>
                                @endfor
                        </div>
                    </div>

                    <div class="row">
                            <div class="mt-3" id="mealtypes"></div>
                    </div>

                    <div class="row">
                        <div class="mt-3" id="recipes"></div>
                    </div>

                    <div class="row">
                        <div class="mt-3" id="choices"></div>
                    </div>
                </div>
            </div>
        <!-- The User has gone through onboarding !-->
        @elseif ($toggle == "fav")
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <form method="POST" enctype="multipart/form-data" id="upload_resume" action="/saveFoods">
                        @csrf        
                        <div class="upload centercomponent mt-3">
                            <div class="upload-files">
                                <header>
                                    <p>
                                        <i class="fa fa-cogs" aria-hidden="true"></i>
                                        <span class="up">Favorite </span>
                                        <span class="load"> Foods</span>
                                    </p>
                                </header>
                        
                                <!-- Breakfast -->
                                <div class="row">
                                    <input type="text" name="breakfast" class="question mt-2" id="breakfast" required autocomplete="off"
                                    @isset($breakfast_fav)
                                        value= {{ $breakfast_fav }}
                                    @endisset
                                    /> 
                                    <label for="breakfast">
                                        <span>Favorite Breakfast ingredients (ex. eggs, avocado)</span>
                                    </label>
                                </div>

                                <!-- Lunch -->
                                <div class="row">
                                    <input type="text" name="lunch" class="question mt-2" id="lunch" required autocomplete="off"
                                    @isset($lunch_fav)
                                        value= {{ $lunch_fav }}
                                    @endisset
                                    /> 
                                    <label for="lunch">
                                        <span>Favorite Lunch ingredients (ex. chicken, kale)</span>
                                    </label>
                                </div>

                                <!-- Dinner -->
                                <div class="row">
                                    <input type="text" name="dinner" class="question mt-2" id="dinner" required autocomplete="off"
                                    @isset($dinner_fav)
                                        value= {{ $dinner_fav }}
                                    @endisset
                                    /> 
                                    <label for="height">
                                        <span>Favorite Dinner ingredients (ex. beef, pork)</span>
                                    </label>
                                </div>

                                <!-- Snack -->
                                <div class="row">
                                    <input type="text" name="snack" class="question mt-2" id="snack" required autocomplete="off"
                                    @isset($snack_fav)
                                        value= {{ $snack_fav }}
                                    @endisset
                                    /> 
                                    <label for="snack">
                                        <span>Favorite Snack ingredients (ex. peanuts, banana)</span>
                                    </label>
                                </div>

                                <div class="row justify-content-center">
                                    <input type="submit" value="Save Settings" onclick="check_errors(event)">
                                </div>

                                <div class="row justify-content-center">
                                    <input type="button" value="Cancel" onclick="goBack()">
                                </div>

                            </div>
                        </div>
                    </form> 
                </div>
            </div>  
        @endif


</div>
@endsection

@section('scripts')

<script type="text/javascript">

    ///Meal
    var meal_array = @json($meals);
        
    //Check if they chose a breakfast yet
    var breakfast = @json($breakfast);

    //Check if they chose a breakfast yet
    var lunch = @json($lunch);

    //Check if they chose a breakfast yet
    var dinner = @json($dinner);

    //Check if they chose a Snack1 yet
    var snack1 = @json($snack1);

    //Check if they chose a Snack2 yet
    var snack2 = @json($snack2);

    function goBack() {

        window.location.href = "/home";
    }

    function showmeals($dayofweek, $month, $day, $numdays) {
        
        $("#mealtypes").html("");

        if ($day > $numdays) {
            $day = $day - $numdays;
            
            var oneMonthLater = new Date(
                new Date().getFullYear(),
                new Date().getMonth() + 1, 
                new Date().getDate()
            );

            $month = oneMonthLater.toLocaleString('default', { month: 'long' });
            $month_num = new Date().getMonth() + 1;
            $month_num++;
        }
        else {
            $month_num = new Date().getMonth();
            $month_num++;
        }

        //Add zero infront
        $month_num = padDigits($month_num);
        $day = padDigits($day);

        $year = new Date().getFullYear();

        $date_format = $year + "-" + $month_num + "-" + $day;
                
        var meal_array = @json($meals);

        if (typeof meal_array[$date_format] !== 'undefined') {

                chooseRecipe($date_format);
        }
        //Display Meal Plans prompt  
        else {
                askPlan($dayofweek, $month, $day, $numdays, $date_format);
        }
    
    }

    /***
     * Display the prompt asking user to choose the meal style they prefer for this day
     **/
    function askPlan($dayofweek, $month, $day, $numdays, $date) {

        $(mealtypes).append('<div class="card"><div class="card-body">Choose your daily meal plan for ' + $dayofweek + ', ' + $month + ' ' + $day);
        $(mealtypes).append('<div class="image" onclick="registerMealPlan(\'' + $date + '\', \'3 Meals\')"><div class="img_3meals" /><div class="text">3 Meals</div></div>');
        $(mealtypes).append('<div class="image" onclick="registerMealPlan(\'' + $date + '\', \'3 Meals, 1 Snack\')"><div class="img_3meals1" /><div class="text">3 Meals + 1 Snack</div></div>');
        $(mealtypes).append('<div class="image" onclick="registerMealPlan(\'' + $date + '\', \'3 Meals, 2 Snacks\')"><div class="img_3meals2" /><div class="text">3 Meals + 2 Snacks</div></div>');
        $(mealtypes).append('</div>');
        $(mealtypes).append('</div>');
    }

    /***
     * User gets to this view if they have already selected a Meal Plan - now they are choosing which recipe they will prepare
     **/
     function chooseRecipe($date) {

        ///Meal
        var meal_choice = meal_array[$date];

        $("#mealtypes").html("");

        //No Breakfast selected yet
        if (typeof breakfast[$date] === 'undefined') {

            var image = "{{ url('/images/breakfast.jpg' ) }}";

            $("#mealtypes").append('<div class="card"><div class="card-body">Add a breakfast for ' + $date);
            $("#mealtypes").append('<div class="image"><img class="img_food" src="' + image + '" onclick="recommendMeal(\'' + $date + '\', \'breakfast\')"></img></div>');
            $("#mealtypes").append("</div>");
            $("#mealtypes").append("</div>");
        }
        //No Lunch selected yet
        else if (typeof lunch[$date] === 'undefined') {

            var image = breakfast[$date]['image_url'];
            var title = breakfast[$date]['title'];
            var type = breakfast[$date]['type'];
            var url = breakfast[$date]['url'];

            //Show Breakfast
            $("#mealtypes").append("<div class='card'><div class='card-body'>Add a lunch for " + $date);
            $("#mealtypes").append('<div class="image"><img class="img_meal" src="' + image + '" onclick="openrecipe(\'' + url + '\')><div class="img_meal_text">' + title + '<br/>' +  type + '</div></img>');  
            $("#mealtypes").append("</div>");

            image = "{{ url('/images/lunch.jpg' ) }}";

            $("#mealtypes").append('<div class="image"><img class="img_food" src="' + image + '" onclick="recommendMeal(\'' + $date + '\', \'lunch\')"></img></div>');
            
            $("#mealtypes").append("</div>");
            $("#mealtypes").append("</div>");
        }
        //No Dinner selected yet
        else if (typeof dinner[$date] === 'undefined') {

            var image = breakfast[$date]['image_url'];
            var title = breakfast[$date]['title'];
            var type = breakfast[$date]['type'];

            //Show Breakfast
            $("#mealtypes").append("<div class='card'><div class='card-body'>Add a dinner for " + $date);
            $("#mealtypes").append('<div class="image"><img class="img_meal" src="' + image + '" onclick="openRecipe(\'' + url + '\')"><div class="img_meal_text">' + title + '<br/>' +  type + '</div></img>');  
            $("#mealtypes").append("</div>");

            image = lunch[$date]['image_url'];
            title = lunch[$date]['title'];
            type = lunch[$date]['type'];

            //Show Lunch
            $("#mealtypes").append('<div class="image"><img class="img_meal" src="' + image + '" onclick="openRecipe(\'' + url + '\')"><div class="img_meal_text">' + title + '<br/>' +  type + '</div></img>');  
            $("#mealtypes").append("</div>");

            image = "{{ url('/images/dinner.jpg' ) }}";

            //Dinner
            $("#mealtypes").append('<div class="image"><img class="img_food" src="' + image + '" onclick="recommendMeal(\'' + $date + '\', \'dinner\')"></img></div>');
            
            $("#mealtypes").append("</div>");
            $("#mealtypes").append("</div>");
        }
        //No Snack selected yet
        else if ( (typeof snack1[$date] === 'undefined') && (meal_choice !== "3 Meals") ) {

            var image = breakfast[$date]['image_url'];
            var title = breakfast[$date]['title'];
            var type = breakfast[$date]['type'];

            //Show Breakfast
            $("#mealtypes").append("<div class='card'><div class='card-body'>Add a snack for " + $date);
            $("#mealtypes").append('<div class="image"><img class="img_meal" src="' + image + '" onclick="openRecipe(\'' + url + '\')"><div class="img_meal_text">' + title + '<br/>' +  type + '</div></img>');  
            $("#mealtypes").append("</div>");

            image = lunch[$date]['image_url'];
            title = lunch[$date]['title'];
            type = lunch[$date]['type'];

            //Show Lunch
            $("#mealtypes").append('<div class="image"><img class="img_meal" src="' + image + '" onclick="openRecipe(\'' + url + '\')"><div class="img_meal_text">' + title + '<br/>' +  type + '</div></img>');  
            $("#mealtypes").append("</div>");

            image = dinner[$date]['image_url'];
            title = dinner[$date]['title'];
            type = dinner[$date]['type'];

            //Show Dinner
            $("#mealtypes").append('<div class="image"><img class="img_meal" src="' + image + '" onclick="openRecipe(\'' + url + '\')"><div class="img_meal_text">' + title + '<br/>' +  type + '</div></img>');  
            $("#mealtypes").append("</div>");

            var image = "{{ url('/images/snack.jpg' ) }}";

            $("#mealtypes").append('<div class="image"><img class="img_food" src="' + image + '" onclick="recommendMeal(\'' + $date + '\', \'snack1\')"></img></div>');
            
            $("#mealtypes").append("</div>");
            $("#mealtypes").append("</div>");
        }
        //No Snack selected yet
        else if ( (typeof snack2[$date] === 'undefined') && (meal_choice === "3 Meals, 2 Snacks") ) {

            var image = breakfast[$date]['image_url'];
            var title = breakfast[$date]['title'];
            var type = breakfast[$date]['type'];

            //Show Breakfast
            $("#mealtypes").append("<div class='card'><div class='card-body'>Add a second snack for " + $date);
            $("#mealtypes").append('<div class="image"><img class="img_meal" src="' + image + '" onclick="openRecipe(\'' + url + '\')"><div class="img_meal_text">' + title + '<br/>' +  type + '</div></img>');  
            $("#mealtypes").append("</div>");

            image = lunch[$date]['image_url'];
            title = lunch[$date]['title'];
            type = lunch[$date]['type'];

            //Show Lunch
            $("#mealtypes").append('<div class="image"><img class="img_meal" src="' + image + '" onclick="openRecipe(\'' + url + '\')"><div class="img_meal_text">' + title + '<br/>' +  type + '</div></img>');  
            $("#mealtypes").append("</div>");

            image = dinner[$date]['image_url'];
            title = dinner[$date]['title'];
            type = dinner[$date]['type'];

            //Show Dinner
            $("#mealtypes").append('<div class="image"><img class="img_meal" src="' + image + '" onclick="openRecipe(\'' + url + '\')"><div class="img_meal_text">' + title + '<br/>' +  type + '</div></img>');  
            $("#mealtypes").append("</div>");

            image = snack1[$date]['image_url'];
            title = snack1[$date]['title'];
            type = snack1[$date]['type'];

            //Show Dinner
            $("#mealtypes").append('<div class="image"><img class="img_meal" src="' + image + '" onclick="openRecipe(\'' + url + '\')"><div class="img_meal_text">' + title + '<br/>' +  type + '</div></img>');  
            $("#mealtypes").append("</div>");

            var image = "{{ url('/images/snack.jpg' ) }}";

            $("#mealtypes").append('<div class="image"><img class="img_food" src="' + image + '" onclick="recommendMeal(\'' + $date + '\', \'snack1\')"></img></div>');
            
            $("#mealtypes").append("</div>");
            $("#mealtypes").append("</div>");
        }  
        //All meals have been added
        else { 

            var image = breakfast[$date]['image_url'];
            var title = breakfast[$date]['title'];
            var type = breakfast[$date]['type'];
            var url = breakfast[$date]['url'];

            //Show Breakfast
            $("#mealtypes").append("<div class='card'><div class='card-body'>Meal Plan for " + $date);
            $("#mealtypes").append('<div class="image"><img class="img_meal" src="' + image + '" onclick="openRecipe(\'' + url + '\')"><div class="img_meal_text">' + title + '<br/>' +  type + '</div></img>');  
            $("#mealtypes").append("</div>");

            image = lunch[$date]['image_url'];
            title = lunch[$date]['title'];
            type = lunch[$date]['type'];
            url = lunch[$date]['url'];


            //Show Lunch
            $("#mealtypes").append('<div class="image"><img class="img_meal" src="' + image + '" onclick="openRecipe(\'' + url + '\')"><div class="img_meal_text">' + title + '<br/>' +  type + '</div></img>');  
            $("#mealtypes").append("</div>");

            image = dinner[$date]['image_url'];
            title = dinner[$date]['title'];
            type = dinner[$date]['type'];
            url = dinner[$date]['url'];

            //Show Dinner
            $("#mealtypes").append('<div class="image"><img class="img_meal" src="' + image + '" onclick="openRecipe(\'' + url + '\')"><div class="img_meal_text">' + title + '<br/>' +  type + '</div></img>');  
            $("#mealtypes").append("</div>");

            if (typeof snack1[$date] !== 'undefined') {
                image = snack1[$date]['image_url'];
                title = snack1[$date]['title'];
                type = snack1[$date]['type'];
                url = snack1[$date]['url'];

                //Show Snack1
                $("#mealtypes").append('<div class="image"><img class="img_meal" src="' + image + '" onclick="openRecipe(\'' + url + '\')"><div class="img_meal_text">' + title + '<br/>' +  type + '</div></img>');  
                $("#mealtypes").append("</div>");
            }

            if (typeof snack2[$date] !== 'undefined') {
                image = snack2[$date]['image_url'];
                title = snack2[$date]['title'];
                type = snack2[$date]['type'];
                url = snack2[$date]['url'];

                //Show Snack2
                $("#mealtypes").append('<div class="image"><img class="img_meal" src="' + image + '" onclick="openRecipe(\'' + url + '\')"><div class="img_meal_text">' + title + '<br/>' +  type + '</div></img>');  
                $("#mealtypes").append("</div>");
            }

            $("#mealtypes").append("</div>");
            $("#mealtypes").append("</div>");

        }      
    }

    /***
     * User Clicks on a Meal Plan
     **/
    function registerMealPlan ($date, $plan) {

        var formData = new FormData();
        formData.append("date", $date);
        formData.append("type", $plan); 

        $.ajax({
            type:'POST',
            url: "{{ url('selectMealPlan')}}",               
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: (data) => {
                chooseRecipe($date);
            },
            error: function(data){
                console.log(data);
            }
        });
    }

    /**
    * Finds a Breakfast
    **/
   function recommendMeal ($date, $meal_type) {
        
        var formData = new FormData();
        formData.append("date", $date);
        formData.append("type", $meal_type);
        
        $.ajax({
            type:'POST',
            url: "{{ url('r_breakfast')}}",               
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: (data) => {

                $("#choices").html("");
                
                $(choices).append('<div class="card"><div class="card-body">Which one of these 3 breakfasts would you like to make?');
                $(choices).append('<div class="image"><img class="img_choices" src="' + data[0]['image_url'] + '" onclick="selectmeal(\'' + data[0]['id'] + '\', \'' + data[0]['dailyID'] + '\', \'' + $meal_type + '\')"/><div class="img_choice_text">' + data[0]['title'] + '<br/>' + Number(data[0]['calories']).toFixed(2) + ' Calories </div></div>');
                $(choices).append('<div class="image"><img class="img_choices" src="' + data[1]['image_url'] + '" onclick="selectmeal(\'' + data[1]['id'] + '\', \'' + data[0]['dailyID'] + '\', \'' + $meal_type + '\')"/><div class="img_choice_text">' + data[1]['title'] + '<br/>' + Number(data[1]['calories']).toFixed(2) + ' Calories </div></div>');
                $(choices).append('<div class="image"><img class="img_choices" src="' + data[2]['image_url'] + '" onclick="selectmeal(\'' + data[2]['id'] + '\', \'' + data[0]['dailyID'] + '\', \'' + $meal_type + '\')"/><div class="img_choice_text">' + data[2]['title'] + '<br/>' + Number(data[2]['calories']).toFixed(2) + ' Calories </div></div>');
                $(choices).append('</div>');
                $(choices).append('</div>');

                //console.log(data);
            },
            error: function(data){
                console.log(data);
            }
        });
   } 

   /**
    * Finds a Breakfast
    **/
    function selectmeal ($recipeID, $dailyID, $type) {

        var formData = new FormData();
        formData.append("recipeID", $recipeID);
        formData.append("dailyID", $dailyID);
        formData.append("type", $type);
        
        $.ajax({
            type:'POST',
            url: "{{ url('select_meal')}}",               
            data: formData,
            cache:false,
            contentType: false,
            processData: false,
            success: (data) => {

                $("#mealtypes").html("");
                $("#choices").html("");

                var count = 0;

                //Number of Meals
                if (data[0]["meal_plan"] === "3 Meals") {
                    count = 3;
                }
                else if (data[0]["meal_plan"] === "3 Meals, 1 Snack") {
                    count = 4;
                }
                else {
                    count = 5;
                }

                var type_of_meal = data[data.length - 1]["type"];
                var meal_date = data[data.length - 1]["active_date"];
                var next_meal = "";

                if (type_of_meal === "breakfast") {
                    next_meal = "Add a lunch for " + meal_date;
                }
                else if (type_of_meal === "lunch") {
                    next_meal = "Add a dinner for " + meal_date;
                }
                else if (type_of_meal === "dinner") {
                    
                    if (count > 3) {
                        next_meal = "Add a first snack for " + meal_date;
                    }
                    else {
                        next_meal = "Meal Plan for " + meal_date;
                    }
                }
                else if (type_of_meal === "snack1") {
                    if (count > 4) {
                        next_meal = "Add a second snack for " + meal_date;
                    }
                    else {
                        next_meal = "Meal Plan for " + meal_date;
                    }
                }

                $(mealtypes).append('<div class="card">' + next_meal);
                $(mealtypes).append('<div class="card-body">');    

                data.forEach(meal => {

                    console.log(meal);

                    //add to breakfast array
                    if (meal['type'] === "breakfast") {
                        breakfast[meal['active_date']] = meal;
                    }
                    //add to lunch array
                    else if (meal['type'] === "lunch") {
                        lunch[meal['active_date']] = meal;
                    }
                    //add to dinner array
                    else if (meal['type'] === "dinner") {
                        dinner[meal['active_date']] = meal;
                    }
                    //add to snack1 array
                    else if (meal['type'] === "snack1") {
                        snack2[meal['active_date']] = meal;
                    }
                    //add to snack2 array
                    else if (meal['type'] === "snack2") {
                        snack2[meal['active_date']] = meal;
                    }

                    $(mealtypes).append('<div class="image"><img class="img_meal" src="' + meal['image_url'] + '" onclick="openrecipe(\'' + meal['url'] + '\')"><div class="img_meal_text">' + meal['title'] + '<br/>' +  meal['type'] + '</div></img>');  
                    $(mealtypes).append("</div>");
                });

                if (type_of_meal === "breakfast") {

                    var image = "{{ url('/images/lunch.jpg' ) }}";

                    $("#mealtypes").append('<div class="image"><img class="img_food" src="' + image + '" onclick="recommendMeal(\'' + meal_date + '\', \'lunch\')"></img></div>');
                    $("#mealtypes").append("</div>");
                }
                else if (type_of_meal === "lunch") {

                    var image = "{{ url('/images/dinner.jpg' ) }}";

                    $("#mealtypes").append('<div class="image"><img class="img_food" src="' + image + '" onclick="recommendMeal(\'' + meal_date + '\', \'dinner\')"></img></div>');
                    $("#mealtypes").append("</div>");
                }
                else if (type_of_meal === "dinner") {
                    
                    if (count > 3) {

                        var image = "{{ url('/images/snack.jpg' ) }}";

                        $("#mealtypes").append('<div class="image"><img class="img_food" src="' + image + '" onclick="recommendMeal(\'' + meal_date + '\', \'snack1\')"></img></div>');
                        $("#mealtypes").append("</div>");
                    }
                }
                else if (type_of_meal === "snack1") {
                    if (count > 4) {
                        var image = "{{ url('/images/snack.jpg' ) }}";

                        $("#mealtypes").append('<div class="image"><img class="img_food" src="' + image + '" onclick="recommendMeal(\'' + meal_date + '\', \'snack2\')"></img></div>');
                        $("#mealtypes").append("</div>");
                    }
                }

                $(mealtypes).append("</div>");
                $(mealtypes).append("</div>");

            },
            error: function(data){
                console.log(data);
            }
        });
    }
   
    /**
    * Finds a Open
    **/
    function openRecipe ($link) {
        window.open($link);
    }

    /***
     * Add a 0 infront of the digit 
     **/
    function padDigits (value) {

        //Pad 0
        if (value < 10) { 
            value = '0' + value; 
        }

        return value;
    }

</script>


@endsection

