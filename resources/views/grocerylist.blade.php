@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">{{ __('Grocery List') }}</div>

            <div class="card-body">
                @foreach ($groceries as $grocery)
                    <div class="row">
                        <input type="checkbox" id="glist" name="glist">
                        <label for="glist">{{ $grocery->content }}</label><br>
                    </div>
                @endforeach
            </div>

            <div class="row justify-content-center">
                <input type="button" value="Go Back" onclick="goBack()">
            </div>

        </div>
    </div>
@endsection

@section('scripts')

    <script type="text/javascript">

        function goBack() {

            window.location.href = "/home";
        }
    
    </script>

@endsection
